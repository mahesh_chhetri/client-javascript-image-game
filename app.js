var $ = document.querySelector.bind(document);
var $$ = document.querySelectorAll.bind(document);
var color1 = "";
var color2 = "";
var color3 = "";

function Game() {
    this.counter = 0;
}

Game.prototype.slideImages = function(container, type, index) {
    console.log(container, type, index)
    this.carouselImages = container.querySelectorAll('.carousel-slide img');
    const size = this.carouselImages[0].clientWidth;
    container.style.transform = 'translateY(' + (-size * this.counter) + 'px)';


    if (type === 'prev') {
        if (this.counter >= this.carouselImages.length - 1) return;
        container.style.transition = "transform 0.4s ease-in-out";
        this.counter++;
        container.style.transform = 'translateY(' + (-size * this.counter - (5 * this.counter)) + 'px)';
    }

    if (type === 'next') {
        if (this.counter <= 0) return;
        container.style.transition = "transform 0.4s ease-in-out";
        this.counter--;
        container.style.transform = 'translateY(' + (-size * this.counter - (5 * this.counter)) + 'px)';
    }

    container.addEventListener('transitionend', () => {
        this.matchColor(index);
        if (this.carouselImages[this.counter].id === 'lastClone') {
            container.style.transition = "none";
            this.counter = this.carouselImages.length - 2;
            container.style.transform = 'translateY(' + (-size * this.counter - (5 * this.counter)) + 'px)';

        }
        if (this.carouselImages[this.counter].id === 'firstClone') {
            container.style.transition = "none";
            this.counter = this.carouselImages.length - this.counter;
            container.style.transform = 'translateY(' + (-size * this.counter) + 'px)';
        }
    });
}

Game.prototype.matchColor = function(index) {
    var color = this.carouselImages[this.counter].getAttribute('color');
    var borderDiv = document.querySelector(".highlight");
    if (index == 0) {
        color1 = color;
    } else if (index == 1) {
        color2 = color;
    } else if (index == 2) {
        color3 = color;
    }

    if ((color1 == color2) && (color1 == color3)) {
        var customHeight = document.querySelector(".content-main").clientHeight - 79;
        console.log("Height", customHeight);
        borderDiv.style.height = customHeight + "px";
        borderDiv.classList.add('active');
        console.log("mach");
    } else {
        console.log("NOT");
        borderDiv.classList.remove('active');
    }
}


// Event listners 

var previousButtons = $$(".prevBtn");
previousButtons.forEach(function(prev) {
    var game = new Game();
    prev.addEventListener('click', function(e) {
        e.preventDefault();
        var container = this.parentElement.querySelector('.carousel-slide');
        var index = this.parentElement.parentElement.getAttribute('data-index');
        game.slideImages(container, 'prev', index);
    });
});


var nextButtons = $$(".nextBtn");
nextButtons.forEach(function(next) {
    var game2 = new Game();
    next.addEventListener('click', function(e) {
        e.preventDefault();
        var container = this.parentElement.querySelector('.carousel-slide');
        var index = this.parentElement.parentElement.getAttribute('data-index');
        game2.slideImages(container, 'next', index);
    });
});

// var conentHeight = $(".content-main").clientHeight;
// var highlightHeight = conentHeight - 8;
// $(".highlight").style.height = highlightHeight + "px";